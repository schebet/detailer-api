'use strict'
const Intern = use('App/Models/Intern')
const { validate } = use('Validator')
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with interns
 */



let interns = [
  {
    id: 1,
    name: "Stacy Ann",
    email: "schebet@sendy.com",
    stack: ["Flutter", "Adonis"]

  },
  {
    id: 2,
    name: "Dorcas Cherono",
    email: "dorcas@sendy.com",
    stack: ["Vue Js", "Adonis"]

  },
  {
    id: 3,
    name: 'Maxwell Kimaiyo',
    email: "maxwell@sendy.com",
    stack: ["Vue JS", "Go"]

  },
  {
    id: 4,
    name: "Gill Erick",
    email: "erick@sendy.com",
    stack: ["Flutter", "Quarkus"]

  }
];

class InternController {

  /**
   * Show a list of all interns.
   * GET interns
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {


    response.json({
      message: "Here's your peeps",
      data: interns
    })
  }

  /**
   * Render a form to be used for creating a new intern.
   * GET interns/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {

  }

  /**
   * Create/save a new intern.
   * POST interns
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {


    var data = request.all()
    interns.push(data)
    response.json({
      message: "Here's your peeps",
      data: interns
    })


  }

  /**
   * Display a single intern.
   * GET interns/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ response, params: { id } }) {

    const intern = interns.find(intern => intern.id == id);

    response.json({
      message: "Here's your peeps",
      data: intern
    })


  }

  /**
   * Render a form to update an existing intern.
   * GET interns/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update intern details.
   * PUT or PATCH interns/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {

    const data = request.only(['name', 'email', 'stack'])
    const internId = Number(params.id) //transform to number
    const interndata = {
      id: internId,
      name: data.name,
      email: data.email,
      stack: data.stack,
    }
    interns = interns.map((item) => {
      return item.id === internId ? interndata : item
    })
    return interndata

  }

  /**
   * Delete a intern with id.
   * DELETE interns/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params: { id }, request, response }) {

    const internId = Number(id);
    interns = interns.filter((intern) => intern.id !== internId);
    response.json({
      message: "Deleted successfully",
      data: interns
    })

  }
}

module.exports = InternController
